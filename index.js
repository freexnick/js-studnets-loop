//1. თითოეული სტუდენტისთვის შექმნათ შესაბამისი მასივი და მოცემული ინფორმაცია ჩაწეროთ შიგნით,
const result = {
  subjects: {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3,
  },
  students: [
    {
      personalInfo: {
        name: "Jean",
        lastName: "Reno",
        age: 26,
      },
      grades: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90,
      },
    },
    {
      personalInfo: {
        name: "Klod",
        lastName: "Mone",
        age: 19,
      },
      grades: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67,
      },
    },
    {
      personalInfo: {
        name: "Van",
        lastName: "Gogh",
        age: 21,
      },
      grades: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70,
      },
    },
    {
      personalInfo: {
        name: "Dam",
        lastName: "Square",
        age: 36,
      },
      grades: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65,
      },
    },
  ],
};

//2. თითოეული სტუდენტისთვის უნდა შეაგროვოთ (გამოთვალოთ) შემდეგი ინფორმაცია: ქულების ჯამი, ქულების საშუალო, GPA. ზემოთხსენებული მასივი ამ ინფორმაციასაც უნდა მოიცავდეს.
const allSubjArr = [
  result.subjects.javascript,
  result.subjects.react,
  result.subjects.python,
  result.subjects.java,
];

let subjCreditSum = 0;
for (subject in result.subjects) {
  subjCreditSum += result.subjects[subject];
}

for (student of result.students) {
  let sum = 0;
  let gpa = 0;
  for (const grade in student.grades) {
    sum += student.grades[grade];
    if (student.grades[grade] >= 51 && student.grades[grade] < 61) {
      gpa += 0.5 * result.subjects[grade];
    } else if (student.grades[grade] >= 61 && student.grades[grade] < 71) {
      gpa += result.subjects[grade];
    } else if (student.grades[grade] >= 71 && student.grades[grade] < 81) {
      gpa += 2 * result.subjects[grade];
    } else if (student.grades[grade] >= 81 && student.grades[grade] < 91) {
      gpa += 3 * result.subjects[grade];
    } else {
      gpa += 4 * result.subjects[grade];
    }
  }
  student.grades.sum = sum;
  student.grades.avg = sum / allSubjArr.length;
  student.grades.gpa = gpa / subjCreditSum;
}

//3. გამოთვალეთ 4 - ვე სტუდენტის საშუალო არითმეტიკული (ანუ ვაფშე ყველა ქულა და ყველა საგანი), სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ "წითელი დიპლომის მქონეს" სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი
let sumOfAllpoints = 0;
for (student of result.students) {
  sumOfAllpoints += student.grades.sum;
}

const avgOfAllpoints =
  sumOfAllpoints / (allSubjArr.length * result.students.length);

for (student of result.students) {
  student.grades.avg > avgOfAllpoints
    ? (student.status = "red diploma")
    : (student.status = "public enemy");
}

//4 უნდა გამოავლინოთ საუკეთესო სტუდენტი GPA ს მიხედვით
let studentWithHighestGpa = result.students[0];
for (let i = 1; i < result.students.length; i++) {
  studentWithHighestGpa.grades.gpa < result.students[i].grades.gpa
    ? (studentWithHighestGpa = result.students[i])
    : studentWithHighestGpa;
}

//5. უნდა გამოავლინოთ საუკეთესო სტუდენტი 21 + ასაკში საშუალო ქულების მიხედვით (აქ 21+ იანის ხელით ამორჩევა არ იგულისხმება, უნდა შეამოწმოთ როგორც საშუალო ქულა, ასევე ასაკი)
let bestStudentAbove21 = result.students[0];
for (let i = 1; i < result.students.length; i++) {
  bestStudentAbove21.personalInfo.age &&
  bestStudentAbove21.grades.avg < result.students[i].personalInfo.age &&
  result.students[i].personalInfo.avg
    ? (bestStudentAbove21 = result.students[i])
    : bestStudentAbove21;
}
//6. უნდა გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით (js, react)
const fontEndSubj = [result.subjects.javascript, result.subjects.react];
for (student of result.students) {
  let avg = 0;
  for (grade in student.grades) {
    if (grade === "react" || grade === "javascript") {
      avg += student.grades[grade];
    }
  }
  student.grades.frontEndAvg = avg / fontEndSubj.length;
}

let bestStudentInFrontEnd = result.students[0];
for (let i = 1; i < result.students.length; i++) {
  bestStudentInFrontEnd.grades.frontEndAvg <
  result.students[i].grades.frontEndAvg
    ? (bestStudentInFrontEnd = result.students[i])
    : bestStudentInFrontEnd;
}
//dumping in obj
result.students.avgOfAllpoints = avgOfAllpoints;
result.students.bestStudents = {};
result.students.bestStudents.studentWithHighestGpa = studentWithHighestGpa;
result.students.bestStudents.bestStudentAbove21 = bestStudentAbove21;
result.students.bestStudents.bestStudentInFrontEnd = bestStudentInFrontEnd;

console.log(result);
